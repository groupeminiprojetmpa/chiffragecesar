from tkinter import *
import string

circulaire = False

"""
Fonction chiffrerString
Parametre textSaisie : Chaine de caracteres
return : chaine de caractere chiffree
"""
def chiffrerString(texteSaisie, decalage):
    texteChiffre = ""
    for char in texteSaisie:
        if(circulaire):
            texteChiffre = texteChiffre + chiffrerCirculaireChar(char, decalage)
        else:
            texteChiffre = texteChiffre + chiffrerChar(char, decalage)
    return texteChiffre

"""
Fonction dechiffrerString
Parametre textSaisie : Chaine de caracteres
return : chaine de caractere dechiffree
"""
def dechiffrerString(texteSaisie, decalage):
    texteDechiffre = ""
    for char in texteSaisie:
        if(circulaire):
            texteDechiffre = texteDechiffre + dechiffrerCirculaireChar(char, decalage)
        else:
            texteDechiffre = texteDechiffre + dechiffrerChar(char, decalage)
    return texteDechiffre

"""
Fonction chiffrerChar
Parametre charEntree : caractere
return : caractere chiffree
"""
def chiffrerChar(charEntree, decalage):
    return chr(ord(charEntree)+ decalage)

"""
Fonction dechiffrerChar
 Parametre charEntree : caractere
 return : caractere dechiffree
"""
def dechiffrerChar(charEntree, decalage):
    return chr(ord(charEntree)- decalage)


def chiffrerCirculaireChar(charEntree, decalage):
    if(charEntree == charEntree.upper()):
        alphabet = string.ascii_uppercase
    else:
        alphabet = string.ascii_lowercase
    index = alphabet.index(charEntree)
    alphabet = alphabet[decalage:] + alphabet[:decalage]
    return alphabet[index]

def dechiffrerCirculaireChar(charEntree, decalage):
    if(charEntree == charEntree.upper()):
        alphabet = string.ascii_uppercase
    else:
        alphabet = string.ascii_lowercase
    alphabetCode = alphabet[decalage:]+alphabet[:decalage]
    index = alphabetCode.index(charEntree)
    return alphabet[index]

"""
Fonctions callback_chiffrer
Prend la chaine de caracteres saisie dans le widget de type text
Appel la fonction chiffrerString en passant en parametre la chaine
Efface la precedente et la remplace par celle chiffree
"""
def callback_chiffrer():
    string = chiffrerString(textarea.get("0.0", END).strip(), int(saisie_decalage.get()))
    textarea.delete("0.0", END)
    textarea.insert("0.0", string)

"""
Fonctions callback_dechiffrer
Prend la chaine de caracteres saisie dans le widget de type text
Appel la fonction dechiffrerString en passant en parametre la chaine
Efface la precedente et la remplace par celle chiffree
"""
def callback_dechiffrer():
    string = dechiffrerString(textarea.get("0.0", END).strip(), int(saisie_decalage.get()))
    textarea.delete("0.0", END)
    textarea.insert("0.0", string)

def callback_circulaire():
    global circulaire
    if(circulaire):
        circulaire = False
        btn_circulaire.configure(fg="red")
    else:
        circulaire = True
        btn_circulaire.configure(fg="green")

if __name__ == '__main__':

    #Configuration de la fenetre
    frame = Tk()
    frame.title("Chiffrage de Cesar")
    frame.minsize(500, 600)

    #Configuration de la grille de la fenetre
    frame.rowconfigure(0, weight=2)
    frame.rowconfigure(1, weight=1)
    frame.rowconfigure(2, weight=1)
    frame.columnconfigure(0, weight=1)
    frame.columnconfigure(1, weight=1)

    #Creation du widget de type text
    textarea = Text(frame,
                    wrap='word',
                    bg='black',
                    fg='white')
    textarea.grid(row=0, columnspan=2, padx=10, pady=10, sticky='nsew')

    # Bonton Chiffrer
    btn_chiffrer = Button(frame,
                            compound="left",
                            text='Chiffrer',
                            command=callback_chiffrer,
                            repeatdelay=500, 
                            repeatinterval=500)

    btn_chiffrer.grid(row=1, column=0, padx=90, pady=20, sticky='nsew')

    # Bouton Dechiffrer
    btn_dechiffrer = Button(frame,
                            compound="right",
                            text='Dechiffrer',
                            command=callback_dechiffrer,
                            repeatdelay=500, 
                            repeatinterval=500)

    btn_dechiffrer.grid(row=1, column=1, padx=80, pady=20, sticky='nsew')

    # Bonton Circulaire
    btn_circulaire = Button(frame,
                            compound="left",
                            text='Circulaire',
                            command=callback_circulaire,
                            fg="red")

    btn_circulaire.grid(row=2, column=0, padx=90, pady=20, sticky='nsew')

    saisie_decalage = Entry(frame, width=2)
    saisie_decalage.grid(row=2, column=1, sticky='nsew')

    saisie_decalage.insert(END, "8")
    
    frame.mainloop()