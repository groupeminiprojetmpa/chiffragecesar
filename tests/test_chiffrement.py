import unittest

from chiffrement import chiffrerChar, dechiffrerChar, chiffrerString, dechiffrerString, dechiffrerCirculaireChar, chiffrerCirculaireChar

class TestFonctionsChiffrement(unittest.TestCase):

    def test_chiffrerString(self):
        self.assertEqual(chiffrerString("F", 8), "N")
        self.assertEqual(chiffrerString("ABC", 8), "IJK")
        self.assertEqual(chiffrerString("def", 8), "lmn")

    def test_chiffrerChar(self):
        self.assertEqual(chiffrerChar('a', 8), 'i')
        self.assertEqual(chiffrerChar('B', 8), 'J')
    
    def test_dechiffrerString(self):
        self.assertEqual(dechiffrerString("N", 8), "F")
        self.assertEqual(dechiffrerString("IJK", 8), "ABC")
        self.assertEqual(dechiffrerString("lmn", 8), "def")

    def test_dechiffrerChar(self):
        self.assertEqual(dechiffrerChar('i', 8), 'a')
        self.assertEqual(dechiffrerChar('J', 8), 'B')

    def test_chiffrerCirculaireChar(self):
        self.assertEqual(chiffrerCirculaireChar('a', 8), 'i')
        self.assertEqual(chiffrerCirculaireChar('B', 8), 'J')
        self.assertEqual(chiffrerCirculaireChar('X', 8), 'F')

    def test_dechifferCirculaireChar(self):
        self.assertEqual(dechiffrerCirculaireChar('i', 8), 'a')
        self.assertEqual(dechiffrerCirculaireChar('J', 8), 'B')
        self.assertEqual(dechiffrerCirculaireChar('F', 8), 'X')